import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Integer tableSize;
        Integer number;

        Scanner keyboard = new Scanner(System.in);
        System.out.print("Masukkan angka: ");
        tableSize = keyboard.nextInt();

        for (int i = 1; i<=tableSize; i++) {
            for (int j = 1; j<=tableSize; j++) {
                number = i*j;
                System.out.print(number + "\t");
            }
            System.out.println();
        }

    }
}